## About the project
#### PHP 8.1
#### Laravel 8
#### sqlite is used for testing
#### postman file in public/docs

## Setup
#### create .env ```cp .env.example to .env``` and update the database configs
#### run ```composer install```
#### create the database ```php artisan migrate```
#### populate the database ```php artisan db:seed```
#### run the tests ```vendor/bin/phpunit```
#### up the server ```php artisan serve```
