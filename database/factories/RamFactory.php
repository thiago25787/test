<?php

namespace Database\Factories;

use App\Enums\TypeStorageEnum;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RamFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'storage' => $this->faker->randomElement([4, 8, 16, 32, 64, 96, 128]),
            'type_storage' => $this->faker->randomElement(TypeStorageEnum::toArray()),
            'ddr' => $this->faker->randomElement([3, 4]),
            'user_id' => User::factory(),
        ];
    }
}
