<?php

namespace Database\Factories;

use App\Enums\TypeHddEnum;
use App\Enums\TypeStorageEnum;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class HddFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'quantity' => $this->faker->numberBetween(1, 10),
            'type' => $this->faker->randomElement(TypeHddEnum::toArray()),
            'storage' => $this->faker->randomElement([1, 2, 3, 120, 240, 300, 480, 500, 960]),
            'type_storage' => $this->faker->randomElement(TypeStorageEnum::toArray()),
            'user_id' => User::factory(),
        ];
    }
}
