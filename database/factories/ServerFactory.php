<?php

namespace Database\Factories;

use App\Models\Hdd;
use App\Models\Location;
use App\Models\Model;
use App\Models\Ram;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'model_id' => Model::factory(),
            'ram_id' => Ram::factory(),
            'location_id' => Location::factory(),
            'hdd_id' => Hdd::factory(),
            'currency' => $this->faker->currencyCode(),
            'price' => $this->faker->randomFloat(2, 0, 100000),
            'user_id' => User::factory(),
        ];
    }
}
