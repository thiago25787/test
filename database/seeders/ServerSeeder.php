<?php

namespace Database\Seeders;

use App\Enums\TypeStorageEnum;
use App\Models\Hdd;
use App\Models\Location;
use App\Models\Model;
use App\Models\Ram;
use App\Models\Server;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $servers = json_decode(file_get_contents('./database/servers.json'));
        DB::transaction(function() use ($servers) {
            foreach ($servers as $dataServer) {
                $model = $this->getModel($dataServer->description);
                $hdd = $this->getHdd($dataServer->hdd);
                $ram = $this->getRam($dataServer->ram);
                $location = $this->getLocation($dataServer->location);
                $server = Server::where('model_id', $model->id)
                    ->where('hdd_id', $hdd->id)
                    ->where('ram_id', $ram->id)
                    ->where('location_id', $location->id)
                    ->where('currency', $dataServer->currency)
                    ->where('price', $dataServer->price)
                    ->first();
                if (!$server) {
                    Server::create([
                        'model_id' => $model->id,
                        'hdd_id' => $hdd->id,
                        'ram_id' => $ram->id,
                        'location_id' => $location->id,
                        'price' => $dataServer->price,
                        'currency' => $dataServer->currency,
                    ]);
                }
            }
        });
    }

    private function getModel(string $description): Model {
        $model = Model::where('description', $description)->first();
        if (!$model) {
            $model = Model::create([
                'description' => $description,
            ]);
        }
        return $model;
    }

    private function getHdd(string $dataHdd): Hdd {
        $quantity = strstr($dataHdd, 'x', true);
        $storage = strstr(str_replace('x', '', strstr($dataHdd, 'x')), TypeStorageEnum::GB->value, true);
        $type = str_replace(TypeStorageEnum::GB->value, '', strstr($dataHdd, TypeStorageEnum::GB->value));
        $typeStorage = TypeStorageEnum::GB->value;
        if (!$storage) {
            $storage = strstr(str_replace('x', '', strstr($dataHdd, 'x')), TypeStorageEnum::TB->value, true);
            $typeStorage = TypeStorageEnum::TB->value;
        }
        if (!$type) {
            $type = str_replace(TypeStorageEnum::TB->value, '', strstr($dataHdd, TypeStorageEnum::TB->value));
        }
        $hdd = Hdd::where('quantity', $quantity)
            ->where('storage', $storage)
            ->where('type_storage', $typeStorage)
            ->where('type', $type)
            ->first();
        if (!$hdd) {
            $hdd = Hdd::create([
                'quantity' => $quantity,
                'storage' => $storage,
                'type_storage' => $typeStorage,
                'type' => $type,
            ]);
        }
        return $hdd;
    }

    private function getRam(string $dataRam): Ram
    {
        $storage = strstr($dataRam, TypeStorageEnum::GB->value, true);
        $typeStorage = TypeStorageEnum::GB->value;
        $ddr = str_replace('DDR', '', strstr($dataRam, 'DDR'));
        if (!$storage) {
            $storage = strstr($dataRam, TypeStorageEnum::TB->value);
            $typeStorage = TypeStorageEnum::TB->value;
        }
        $ram = Ram::where('storage', $storage)
            ->where('type_storage', $typeStorage)
            ->where('ddr', $ddr)
            ->first();
        if (!$ram) {
            $ram = Ram::create([
                'storage' => $storage,
                'type_storage' => $typeStorage,
                'ddr' => $ddr,
            ]);
        }
        return $ram;
    }

    private function getLocation(string $dataLocation): Location
    {
        $location = Location::where('description', $dataLocation)
            ->first();
        if (!$location) {
            $location = Location::create([
                'description' => $dataLocation,
            ]);
        }
        return $location;
    }
}
