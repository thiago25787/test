<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API')->group(function () {
    Route::prefix('v1')->namespace('V1')->group(function () {
//        Route::middleware(['auth:sanctum'])->group(function () {
            Route::apiResource('model', 'ModelController');
            Route::apiResource('hdd', 'HddController');
            Route::apiResource('ram', 'RamController');
            Route::apiResource('location', 'LocationController');
            Route::apiResource('server', 'ServerController');
//        });
    });
});
