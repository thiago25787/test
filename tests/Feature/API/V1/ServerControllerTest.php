<?php

namespace Tests\Feature\API\V1;

use App\Enums\TypeHddEnum;
use App\Enums\TypeStorageEnum;
use App\Models\Hdd;
use App\Models\Location;
use App\Models\Model;
use App\Models\Ram;
use App\Models\Server;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ServerControllerTest extends TestCase
{
    private const TABLE_NAME = 'servers';

    use DatabaseTransactions;

    private User $loggedUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->loggedUser = User::factory()->create();
        Sanctum::actingAs($this->loggedUser);
    }

    public function testListServers(): void
    {
        $servers = Server::factory(10)->create();

        $response = $this->get('/api/v1/server');
        $response->assertOk();
        $responseData = $response->json();
        $this->assertCount(10, $responseData['data']);

        foreach ($servers as $server) {
            $response->assertJsonFragment([
                'model_id' => $server->model_id,
                'ram_id' => $server->ram_id,
                'location_id' => $server->location_id,
                'hdd_id' => $server->hdd_id,
                'currency' => $server->currency,
            ]);
        }
    }

    public function testListServersWithParams(): void
    {
        $model = Model::factory()->create([
            'description' => 'Model description',
        ]);
        $location = Location::factory()->create([
            'description' => 'Location description',
        ]);
        $hdd = Hdd::factory()->create([
            'storage' => 250,
            'type_storage' => TypeStorageEnum::GB,
            'type' => TypeHddEnum::SSD,
        ]);
        $ram = Ram::factory()->create([
            'storage' => 8,
            'type_storage' => TypeStorageEnum::GB,
        ]);
        $serversModel = Server::factory(10)->create([
            'model_id' => $model->id,
            'currency' => '$',
        ]);
        $serversLocation = Server::factory(10)->create([
            'location_id' => $location->id,
            'currency' => '£',
        ]);
        $serversHdd = Server::factory(10)->create([
            'hdd_id' => $hdd->id,
            'currency' => '€',
        ]);
        $serversRam = Server::factory(10)->create([
            'ram_id' => $ram->id,
            'currency' => 'S$',
        ]);

        $responseModel = $this->get('/api/v1/server?model.description=model%20description');
        $responseModel->assertOk();

        foreach ($serversModel as $server) {
            $responseModel->assertJsonFragment([
                'id' => $server->id,
                'model_id' => $server->model_id,
                'ram_id' => $server->ram_id,
                'location_id' => $server->location_id,
                'hdd_id' => $server->hdd_id,
                'currency' => $server->currency,
            ]);
        }

        $responseLocation = $this->get('/api/v1/server?location.description=location%20description');
        $responseLocation->assertOk();
        foreach ($serversLocation as $server) {
            $responseLocation->assertJsonFragment([
                'id' => $server->id,
                'model_id' => $server->model_id,
                'ram_id' => $server->ram_id,
                'location_id' => $server->location_id,
                'hdd_id' => $server->hdd_id,
                'currency' => $server->currency,
            ]);
        }

        $responseHdd = $this->get('/api/v1/server?hdd.storage=250GB');
        $responseHdd->assertOk();

        foreach ($serversHdd as $server) {
            $responseHdd->assertJsonFragment([
                'id' => $server->id,
                'model_id' => $server->model_id,
                'ram_id' => $server->ram_id,
                'location_id' => $server->location_id,
                'hdd_id' => $server->hdd_id,
                'currency' => $server->currency,
            ]);
        }

        $responseRam = $this->get('/api/v1/server?ram.storage=8GB');
        $responseRam->assertOk();

        foreach ($serversRam as $server) {
            $responseRam->assertJsonFragment([
                'id' => $server->id,
                'model_id' => $server->model_id,
                'ram_id' => $server->ram_id,
                'location_id' => $server->location_id,
                'hdd_id' => $server->hdd_id,
                'currency' => $server->currency,
            ]);
        }

        $responseCurrency = $this->get('/api/v1/server?per_page=10&currency=€');
        $responseCurrency->assertOk();
        $responseCurrency->assertJsonCount(10, 'data');

        $responseHddType = $this->get('/api/v1/server?per_page=10&hdd.type=' . TypeHddEnum::SSD->value);
        $responseHddType->assertOk();
        $responseHddType->assertJsonCount(10, 'data');
    }

    public function testShowServer(): void
    {
        $server = Server::factory()->create();
        $response = $this->get('/api/v1/server/' . $server->id);
        $response->assertOk();

        $response->assertJsonFragment([
            'id' => $server->id,
            'model_id' => $server->model_id,
            'ram_id' => $server->ram_id,
            'location_id' => $server->location_id,
            'hdd_id' => $server->hdd_id,
            'currency' => $server->currency,
        ]);
    }

    public function testShowServerShouldFail(): void
    {
        $server = Server::factory()->create();
        $responseWrongId = $this->get('/api/v1/server/' . ($server->id + 1000));
        $responseWrongId->assertNotFound();
    }

    public function testCreateServer(): void
    {
        $server = Server::factory()->make();
        $data = [
            'model_id' => $server->model_id,
            'ram_id' => $server->ram_id,
            'location_id' => $server->location_id,
            'hdd_id' => $server->hdd_id,
            'currency' => $server->currency,
            'price' => $server->price,
        ];
        $response = $this->post('/api/v1/server', $data);
        $response->assertCreated();
        $response->assertJsonFragment($data);

        $this->assertDatabaseHas(self::TABLE_NAME, $data);
    }

    public function testCreateServerShouldFail(): void
    {
        $responseWithoutData = $this->post('/api/v1/server', []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'model_id', 'ram_id', 'location_id', 'hdd_id', 'currency', 'price',
        ]);

        $server = Server::factory()->make();
        $data = [
            'model_id' => $server->model_id,
            'ram_id' => $server->ram_id,
            'location_id' => $server->location_id,
            'hdd_id' => $server->hdd_id,
            'currency' => $server->currency,
            'price' => $server->price,
        ];

        $wrongData = $data;
        $wrongData['currency'] = 0;
        $wrongData['price'] = '';

        $responseWrongData = $this->post('/api/v1/server', $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['currency', 'price']);

        Server::creating(function () {
            throw new \Exception();
        });

        $responseException = $this->post('/api/v1/server', $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testUpdateServer(): void
    {
        $server = Server::factory()->create();
        $response = $this->put('/api/v1/server/' . $server->id, [
            'price' => 200,
            'model_id' => $server->model_id,
            'ram_id' => $server->ram_id,
            'location_id' => $server->location_id,
            'hdd_id' => $server->hdd_id,
            'currency' => $server->currency,
        ]);
        $response->assertOk();
        $response->assertJsonFragment([
            'id' => $server->id,
            'model_id' => $server->model_id,
            'ram_id' => $server->ram_id,
            'location_id' => $server->location_id,
            'hdd_id' => $server->hdd_id,
            'currency' => $server->currency,
            'price' => 200,
        ]);

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $server->id,
            'model_id' => $server->model_id,
            'ram_id' => $server->ram_id,
            'location_id' => $server->location_id,
            'hdd_id' => $server->hdd_id,
            'currency' => $server->currency,
            'price' => 200,
        ]);
    }

    public function testUpdateServerShouldFail(): void
    {
        $server = Server::factory()->create();

        $responseWithoutData = $this->put('/api/v1/server/' . $server->id, []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'model_id', 'ram_id', 'location_id', 'hdd_id', 'currency', 'price',
        ]);
        $data = [
            'price' => 300,
            'currency' => $server->currency,
            'model_id' => $server->model_id,
            'ram_id' => $server->ram_id,
            'location_id' => $server->location_id,
            'hdd_id' => $server->hdd_id,
        ];

        $responseWrongId = $this->put('/api/v1/server/' . ($server->id + 1000), $data);
        $responseWrongId->assertNotFound();

        $wrongData = $data;
        $wrongData['price'] = 0;
        $wrongData['currency'] = 'wrong';

        $responseWrongData = $this->put('/api/v1/server/' . $server->id, $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['price', 'currency']);

        Server::updating(function () {
            throw new \Exception();
        });

        $responseException = $this->put('/api/v1/server/' . $server->id, $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testDeleteServer(): void
    {
        $server = Server::factory()->create();
        $response = $this->delete('/api/v1/server/' . $server->id);
        $response->assertNoContent();

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $server->id,
        ]);
        $this->assertSoftDeleted(self::TABLE_NAME, [
            'id' => $server->id,
        ]);
    }

    public function testDeleteServerShouldFail(): void
    {
        $server = Server::factory()->create();

        $responseWrongId = $this->delete('api/v1/server/' . ($server->id + 1000));
        $responseWrongId->assertNotFound();
        $this->assertNotSoftDeleted(self::TABLE_NAME, ['id' => $server->id]);

        Server::deleting( function () {
            throw new \Exception();
        });

        $responseException = $this->delete('api/v1/server/' . $server->id);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
