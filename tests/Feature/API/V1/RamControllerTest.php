<?php

namespace Tests\Feature\API\V1;

use App\Models\Ram;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class RamControllerTest extends TestCase
{
    private const TABLE_NAME = 'rams';

    use DatabaseTransactions;

    private User $loggedUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->loggedUser = User::factory()->create();
        Sanctum::actingAs($this->loggedUser);
    }

    public function testListRams(): void
    {
        $rams = Ram::factory(10)->create();
        $response = $this->get('/api/v1/ram');
        $response->assertOk();
        $responseData = $response->json();
        $this->assertCount(10, $responseData['data']);

        foreach ($rams as $ram) {
            $response->assertJsonFragment([
                'id' => $ram->id,
                'storage' => $ram->storage,
                'type_storage' => $ram->type_storage->value,
                'ddr' => $ram->ddr,
            ]);
        }
    }

    public function testShowRam(): void
    {
        $ram = Ram::factory()->create();
        $response = $this->get('/api/v1/ram/' . $ram->id);
        $response->assertOk();
        $response->assertJsonFragment([
            'id' => $ram->id,
            'storage' => $ram->storage,
            'type_storage' => $ram->type_storage->value,
            'ddr' => $ram->ddr,
        ]);
    }

    public function testShowRamShouldFail(): void
    {
        $ram = Ram::factory()->create();
        $responseWrongId = $this->get('/api/v1/ram/' . ($ram->id + 1000));
        $responseWrongId->assertNotFound();
    }

    public function testCreateRam(): void
    {
        $ram = Ram::factory()->make();
        $data = [
            'storage' => $ram->storage,
            'type_storage' => $ram->type_storage->value,
            'ddr' => $ram->ddr,
        ];
        $response = $this->post('/api/v1/ram', $data);
        $response->assertCreated();
        $response->assertJsonFragment($data);

        $this->assertDatabaseHas(self::TABLE_NAME, $data);
    }

    public function testCreateRamShouldFail(): void
    {
        $responseWithoutData = $this->post('/api/v1/ram', []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'storage', 'type_storage', 'ddr',
        ]);

        $ram = Ram::factory()->make();
        $data = [
            'storage' => $ram->storage,
            'type_storage' => $ram->type_storage->value,
            'ddr' => $ram->ddr,
        ];

        $wrongData = $data;
        $wrongData['storage'] = 0;
        $wrongData['ddr'] = 0;

        $responseWrongData = $this->post('/api/v1/ram', $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['storage', 'ddr']);

        Ram::creating(function () {
            throw new \Exception();
        });

        $responseException = $this->post('/api/v1/ram', $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testUpdateRam(): void
    {
        $ram = Ram::factory()->create();
        $data = [
            'storage' => 100,
            'type_storage' => $ram->type_storage->value,
            'ddr' => $ram->ddr,
        ];
        $response = $this->put('/api/v1/ram/' . $ram->id, $data);

        $response->assertOk();

        $response->assertJsonFragment([
            'id' => $ram->id,
            'storage' => 100,
            'type_storage' => $ram->type_storage->value,
            'ddr' => $ram->ddr,
        ]);

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $ram->id,
            'storage' => 100,
            'type_storage' => $ram->type_storage->value,
            'ddr' => $ram->ddr,
        ]);
    }

    public function testUpdateRamShouldFail(): void
    {
        $ram = Ram::factory()->create();

        $responseWithoutData = $this->put('/api/v1/ram/' . $ram->id, []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'storage', 'type_storage', 'ddr',
        ]);
        $data = [
            'storage' => 100,
            'type_storage' => $ram->type_storage->value,
            'ddr' => $ram->ddr,
        ];

        $responseWrongId = $this->put('/api/v1/ram/' . ($ram->id + 1000), $data);
        $responseWrongId->assertNotFound();

        $wrongData = $data;
        $wrongData['storage'] = 0;
        $wrongData['ddr'] = 0;

        $responseWrongData = $this->put('/api/v1/ram/' . $ram->id, $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['storage', 'ddr']);

        Ram::updating(function () {
            throw new \Exception();
        });

        $responseException = $this->put('/api/v1/ram/' . $ram->id, $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testDeleteRam(): void
    {
        $ram = Ram::factory()->create();
        $response = $this->delete('/api/v1/ram/' . $ram->id);
        $response->assertNoContent();

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $ram->id,
        ]);
        $this->assertSoftDeleted(self::TABLE_NAME, [
            'id' => $ram->id,
        ]);
    }

    public function testDeleteRamShouldFail(): void
    {
        $ram = Ram::factory()->create();

        $responseWrongId = $this->delete('api/v1/ram/' . ($ram->id + 1000));
        $responseWrongId->assertNotFound();
        $this->assertNotSoftDeleted(self::TABLE_NAME, ['id' => $ram->id]);

        Ram::deleting( function () {
            throw new \Exception();
        });

        $responseException = $this->delete('api/v1/ram/' . $ram->id);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
