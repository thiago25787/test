<?php

namespace Tests\Feature\API\V1;

use App\Models\Location;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class LocationControllerTest extends TestCase
{
    private const TABLE_NAME = 'locations';

    use DatabaseTransactions;

    private User $loggedUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->loggedUser = User::factory()->create();
        Sanctum::actingAs($this->loggedUser);
    }

    public function testListLocations(): void
    {
        $locations = Location::factory(10)->create();
        $response = $this->get('/api/v1/location');
        $response->assertOk();
        $responseData = $response->json();
        $this->assertCount(10, $responseData['data']);

        foreach ($locations as $location) {
            $response->assertJsonFragment([
                'id' => $location->id,
                'description' => $location->description,
            ]);
        }
    }

    public function testShowLocation(): void
    {
        $location = Location::factory()->create();
        $response = $this->get('/api/v1/location/' . $location->id);
        $response->assertOk();
        $response->assertJsonFragment([
            'id' => $location->id,
            'description' => $location->description,
        ]);
    }

    public function testShowLocationShouldFail(): void
    {
        $location = Location::factory()->create();
        $responseWrongId = $this->get('/api/v1/location/' . ($location->id + 1000));
        $responseWrongId->assertNotFound();
    }

    public function testCreateLocation(): void
    {
        $location = Location::factory()->make();
        $data = [
            'description' => $location->description,
        ];
        $response = $this->post('/api/v1/location', $data);
        $response->assertCreated();
        $response->assertJsonFragment($data);

        $this->assertDatabaseHas(self::TABLE_NAME, $data);
    }

    public function testCreateLocationShouldFail(): void
    {
        $responseWithoutData = $this->post('/api/v1/location', []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'description',
        ]);

        $location = Location::factory()->make();
        $data = [
            'description' => $location->description,
        ];

        $wrongData = $data;
        $wrongData['description'] = '';

        $responseWrongData = $this->post('/api/v1/location', $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['description']);

        Location::creating(function () {
            throw new \Exception();
        });

        $responseException = $this->post('/api/v1/location', $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testUpdateLocation(): void
    {
        $location = Location::factory()->create();
        $response = $this->put('/api/v1/location/' . $location->id, [
            'description' => $location->description . '_updated',
        ]);
        $response->assertOk();
        $response->assertJsonFragment([
            'id' => $location->id,
            'description' => $location->description . '_updated',
        ]);

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $location->id,
            'description' => $location->description . '_updated',
        ]);
    }

    public function testUpdateLocationShouldFail(): void
    {
        $location = Location::factory()->create();

        $responseWithoutData = $this->put('/api/v1/location/' . $location->id, []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'description',
        ]);
        $data = [
            'description' => $location->description . '_updated',
        ];

        $responseWrongId = $this->put('/api/v1/location/' . ($location->id + 1000), $data);
        $responseWrongId->assertNotFound();

        $wrongData = $data;
        $wrongData['description'] = '';

        $responseWrongData = $this->put('/api/v1/location/' . $location->id, $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['description']);

        Location::updating(function () {
            throw new \Exception();
        });

        $responseException = $this->put('/api/v1/location/' . $location->id, $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testDeleteLocation(): void
    {
        $location = Location::factory()->create();
        $response = $this->delete('/api/v1/location/' . $location->id);
        $response->assertNoContent();

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $location->id,
        ]);
        $this->assertSoftDeleted(self::TABLE_NAME, [
            'id' => $location->id,
        ]);
    }

    public function testDeleteLocationShouldFail(): void
    {
        $location = Location::factory()->create();

        $responseWrongId = $this->delete('api/v1/location/' . ($location->id + 1000));
        $responseWrongId->assertNotFound();
        $this->assertNotSoftDeleted(self::TABLE_NAME, ['id' => $location->id]);

        Location::deleting( function () {
            throw new \Exception();
        });

        $responseException = $this->delete('api/v1/location/' . $location->id);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
