<?php

namespace Tests\Feature\API\V1;

use App\Models\Hdd;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class HddControllerTest extends TestCase
{
    private const TABLE_NAME = 'hdds';

    use DatabaseTransactions;

    private User $loggedUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->loggedUser = User::factory()->create();
        Sanctum::actingAs($this->loggedUser);
    }

    public function testListHdds(): void
    {
        $hdds = Hdd::factory(10)->create();
        $response = $this->get('/api/v1/hdd');
        $response->assertOk();
        $responseData = $response->json();
        $this->assertCount(10, $responseData['data']);

        foreach ($hdds as $hdd) {
            $response->assertJsonFragment([
                'id' => $hdd->id,
                'quantity' => $hdd->quantity,
                'type' => $hdd->type->value,
                'storage' => $hdd->storage,
                'type_storage' => $hdd->type_storage->value,
            ]);
        }
    }

    public function testShowHdd(): void
    {
        $hdd = Hdd::factory()->create();
        $response = $this->get('/api/v1/hdd/' . $hdd->id);
        $response->assertOk();
        $response->assertJsonFragment([
            'id' => $hdd->id,
            'quantity' => $hdd->quantity,
            'type' => $hdd->type->value,
            'storage' => $hdd->storage,
            'type_storage' => $hdd->type_storage->value,
        ]);
    }

    public function testShowHddShouldFail(): void
    {
        $hdd = Hdd::factory()->create();
        $responseWrongId = $this->get('/api/v1/hdd/' . ($hdd->id + 1000));
        $responseWrongId->assertNotFound();
    }

    public function testCreateHdd(): void
    {
        $hdd = Hdd::factory()->make();
        $data = [
            'quantity' => $hdd->quantity,
            'type' => $hdd->type->value,
            'storage' => $hdd->storage,
            'type_storage' => $hdd->type_storage->value,
        ];
        $response = $this->post('/api/v1/hdd', $data);
        $response->assertCreated();
        $response->assertJsonFragment($data);

        $this->assertDatabaseHas(self::TABLE_NAME, $data);
    }

    public function testCreateHddShouldFail(): void
    {
        $responseWithoutData = $this->post('/api/v1/hdd', []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'quantity', 'type', 'storage', 'type_storage',
        ]);

        $hdd = Hdd::factory()->make();
        $data = [
            'quantity' => $hdd->quantity,
            'type' => $hdd->type->value,
            'storage' => $hdd->storage,
            'type_storage' => $hdd->type_storage->value,
        ];
        $wrongData = $data;
        $wrongData['quantity'] = 0;

        $responseWrongData = $this->post('/api/v1/hdd', $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['quantity']);

        Hdd::creating(function () {
            throw new \Exception();
        });

        $responseException = $this->post('/api/v1/hdd', $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testUpdateHdd(): void
    {
        $hdd = Hdd::factory()->create();
        $data = [
            'quantity' => $hdd->quantity,
            'type' => $hdd->type->value,
            'storage' => $hdd->storage + 100,
            'type_storage' => $hdd->type_storage->value,
        ];

        $response = $this->put('/api/v1/hdd/' . $hdd->id, $data);
        $response->assertOk();
        $response->assertJsonFragment($data);

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $hdd->id,
        ]);
    }

    public function testUpdateHddShouldFail(): void
    {
        $hdd = Hdd::factory()->create();

        $responseWithoutData = $this->put('/api/v1/hdd/' . $hdd->id, []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'quantity', 'type', 'storage', 'type_storage',
        ]);
        $data = [
            'quantity' => $hdd->quantity + 100,
            'type' => $hdd->type->value,
            'storage' => $hdd->storage,
            'type_storage' => $hdd->type_storage->value,
        ];

        $responseWrongId = $this->put('/api/v1/hdd/' . ($hdd->id + 1000), $data);
        $responseWrongId->assertNotFound();

        $wrongData = $data;
        $wrongData['quantity'] = 0;

        $responseWrongData = $this->put('/api/v1/hdd/' . $hdd->id, $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['quantity']);

        Hdd::updating(function () {
            throw new \Exception();
        });

        $responseException = $this->put('/api/v1/hdd/' . $hdd->id, $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }


    public function testDeleteHdd(): void
    {
        $hdd = Hdd::factory()->create();
        $response = $this->delete('/api/v1/hdd/' . $hdd->id);
        $response->assertNoContent();

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $hdd->id,
        ]);
        $this->assertSoftDeleted(self::TABLE_NAME, [
            'id' => $hdd->id,
        ]);
    }

    public function testDeleteHddShouldFail(): void
    {
        $hdd = Hdd::factory()->create();
        $responseWrongId = $this->delete('api/v1/hdd/' . ($hdd->id + 1000));
        $responseWrongId->assertNotFound();
        $this->assertNotSoftDeleted(self::TABLE_NAME, ['id' => $hdd->id]);

        Hdd::deleting( function () {
            throw new \Exception();
        });

        $responseException = $this->delete('api/v1/hdd/' . $hdd->id);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
