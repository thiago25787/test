<?php

namespace Tests\Feature\API\V1;

use App\Models\Model;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ModelControllerTest extends TestCase
{
    private const TABLE_NAME = 'models';

    use DatabaseTransactions;

    private User $loggedUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->loggedUser = User::factory()->create();
        Sanctum::actingAs($this->loggedUser);
    }

    public function testListModels(): void
    {
        $models = Model::factory(10)->create();
        $response = $this->get('/api/v1/model');
        $response->assertOk();
        $responseData = $response->json();
        $this->assertCount(10, $responseData['data']);

        foreach ($models as $model) {
            $response->assertJsonFragment([
                'id' => $model->id,
                'description' => $model->description,
            ]);
        }
    }

    public function testShowModel(): void
    {
        $model = Model::factory()->create();
        $response = $this->get('/api/v1/model/' . $model->id);
        $response->assertOk();
        $response->assertJsonFragment([
            'id' => $model->id,
            'description' => $model->description,
        ]);
    }

    public function testShowModelShouldFail(): void
    {
        $model = Model::factory()->create();
        $responseWrongId = $this->get('/api/v1/model/' . ($model->id + 1000));
        $responseWrongId->assertNotFound();
    }

    public function testCreateModel(): void
    {
        $model = Model::factory()->make();
        $data = [
            'description' => $model->description,
        ];
        $response = $this->post('/api/v1/model', $data);
        $response->assertCreated();
        $response->assertJsonFragment($data);

        $this->assertDatabaseHas(self::TABLE_NAME, $data);
    }

    public function testCreateModelShouldFail(): void
    {
        $responseWithoutData = $this->post('/api/v1/model', []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'description',
        ]);

        $model = Model::factory()->make();
        $data = [
            'description' => $model->description,
        ];

        $wrongData = $data;
        $wrongData['description'] = '';

        $responseWrongData = $this->post('/api/v1/model', $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['description']);

        Model::creating(function () {
            throw new \Exception();
        });

        $responseException = $this->post('/api/v1/model', $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testUpdateModel(): void
    {
        $model = Model::factory()->create();
        $response = $this->put('/api/v1/model/' . $model->id, [
            'description' => $model->description . '_updated',
        ]);
        $response->assertOk();
        $response->assertJsonFragment([
            'id' => $model->id,
            'description' => $model->description . '_updated',
        ]);

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $model->id,
            'description' => $model->description . '_updated',
        ]);
    }

    public function testUpdateModelShouldFail(): void
    {
        $model = Model::factory()->create();

        $responseWithoutData = $this->put('/api/v1/model/' . $model->id, []);
        $responseWithoutData->assertStatus(Response::HTTP_FOUND);
        $responseWithoutData->assertSessionHasErrors([
            'description',
        ]);
        $data = [
            'description' => $model->description . '_updated',
        ];

        $responseWrongId = $this->put('/api/v1/model/' . ($model->id + 1000), $data);
        $responseWrongId->assertNotFound();

        $wrongData = $data;
        $wrongData['description'] = '';

        $responseWrongData = $this->put('/api/v1/model/' . $model->id, $wrongData);
        $responseWrongData->assertStatus(Response::HTTP_FOUND);
        $responseWrongData->assertSessionHasErrors(['description']);

        Model::updating(function () {
            throw new \Exception();
        });

        $responseException = $this->put('/api/v1/model/' . $model->id, $data);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testDeleteModel(): void
    {
        $model = Model::factory()->create();
        $response = $this->delete('/api/v1/model/' . $model->id);
        $response->assertNoContent();

        $this->assertDatabaseHas(self::TABLE_NAME, [
            'id' => $model->id,
        ]);
        $this->assertSoftDeleted(self::TABLE_NAME, [
            'id' => $model->id,
        ]);
    }

    public function testDeleteModelShouldFail(): void
    {
        $model = Model::factory()->create();

        $responseWrongId = $this->delete('api/v1/model/' . ($model->id + 1000));
        $responseWrongId->assertNotFound();
        $this->assertNotSoftDeleted(self::TABLE_NAME, ['id' => $model->id]);

        Model::deleting( function () {
            throw new \Exception();
        });

        $responseException = $this->delete('api/v1/model/' . $model->id);
        $responseException->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
