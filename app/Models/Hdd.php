<?php

namespace App\Models;

use App\Enums\TypeHddEnum;
use App\Enums\TypeStorageEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hdd extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'quantity',
        'type',
        'storage',
        'type_storage',
        'user_id',
    ];

    protected $casts = [
        'type' => TypeHddEnum::class,
        'type_storage' => TypeStorageEnum::class,
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
