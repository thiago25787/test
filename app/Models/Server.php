<?php

namespace App\Models;

use App\Enums\TypeStorageEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;

class Server extends EloquentModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'model_id',
        'ram_id',
        'location_id',
        'hdd_id',
        'currency',
        'price',
        'user_id',
    ];

    public function model()
    {
        return $this->belongsTo(Model::class);
    }

    public function ram()
    {
        return $this->belongsTo(Ram::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function hdd()
    {
        return $this->belongsTo(Hdd::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function search(array $data): LengthAwarePaginator
    {
        $servers = self::query()
            ->select('servers.*')
            ->when($data['model_description'] ?? null, function ($query) use ($data) {
                $query->join('models', 'servers.model_id', '=', 'models.id');
                $query->whereNull('models.deleted_at');
                return $query->whereRaw("lower(models.description) like lower('%{$data['model_description']}%')");
            })
            ->when($data['currency'] ?? null, function ($query) use ($data) {
                return $query->where('currency', '=', $data['currency']);
            })
            ->when($data['location_description'] ?? null, function ($query) use ($data) {
                $query->join('locations', 'servers.location_id', '=', 'locations.id');
                $query->whereNull('locations.deleted_at');
                return $query->whereRaw("lower(locations.description) like lower('%{$data['location_description']}%')");
            })
            ->when(($data['ram_ddr'] ?? null) || ($data['ram_storage'] ?? null), function ($query) use ($data) {
                $query->join('rams', 'servers.ram_id', '=', 'rams.id');
                $query->whereNull('rams.deleted_at');
                $query->when($data['ram_ddr'] ?? null, function ($q) use ($data) {
                    return $q->where('rams.ddr', '=', $data['ram_ddr']);
                });
                $query->when($data['ram_storage'] ?? null, function ($q) use ($data) {
                    $storage = Str::onlyNumbers($data['ram_storage']);
                    $typeStorage = Str::withoutNumbers($data['ram_storage']);
                    return $q->where('rams.storage', '=', $storage)
                        ->where('rams.type_storage', '=', TypeStorageEnum::from($typeStorage));
                });
            })
            ->when(($data['hdd_storage'] ?? null) || ($data['hdd_type'] ?? null), function ($query) use ($data) {
                $query->join('hdds', 'servers.hdd_id', '=', 'hdds.id');
                $query->whereNull('hdds.deleted_at');
                $query->when($data['hdd_storage'] ?? null, function ($q) use ($data) {
                    $storage = Str::onlyNumbers($data['hdd_storage']);
                    $typeStorage = Str::withoutNumbers($data['hdd_storage']);
                    return $q->where('hdds.storage', '=', $storage)
                        ->where('hdds.type_storage', '=', TypeStorageEnum::from($typeStorage));;
                });
                $query->when($data['hdd_type'] ?? null, function ($q) use ($data) {
                    return $q->where('hdds.type', '=', $data['hdd_type']);
                });
            })->with(['hdd', 'ram', 'location', 'model'])
            ->orderBy('servers.created_at', 'desc')
            ->paginate($data['per_page'] ?? 50);
        return $servers;
    }
}
