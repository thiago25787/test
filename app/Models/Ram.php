<?php

namespace App\Models;

use App\Enums\TypeStorageEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ram extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'storage',
        'type_storage',
        'ddr',
        'user_id',
    ];

    protected $casts = [
        'type_storage' => TypeStorageEnum::class
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
