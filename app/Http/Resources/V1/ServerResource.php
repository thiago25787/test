<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class ServerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'model_id' => $this->model_id,
            'ram_id' => $this->ram_id,
            'location_id' => $this->location_id,
            'hdd_id' => $this->hdd_id,
            'currency' => $this->currency,
            'price' => $this->price,
            'model' => new ModelResource($this->whenLoaded('model')),
            'ram' => new RamResource($this->whenLoaded('ram')),
            'location' => new LocationResource($this->whenLoaded('location')),
            'hdd' => new HddResource($this->whenLoaded('hdd')),
        ];
    }
}
