<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class HddResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'quantity' => $this->quantity,
            'type' => $this->type->value,
            'storage' => $this->storage,
            'type_storage' => $this->type_storage->value,
        ];
    }
}
