<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class RamResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'storage' => $this->storage,
            'type_storage' => $this->type_storage->value,
            'ddr' => $this->ddr,
        ];
    }
}
