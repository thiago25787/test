<?php

namespace App\Http\Requests;

use App\Enums\TypeHddEnum;
use Illuminate\Validation\Rules\Enum;

class ServerListRequest extends AbstractRequest
{
    public function rules()
    {
        return [
            'hdd.storage' => ['string'],
            'hdd.type' => [new Enum(TypeHddEnum::class)],
            'location.description' => ['string', 'max:255'],
            'model.description' => ['string', 'max:255'],
            'ram.ddr' => ['string', 'max:255'],
            'ram.storage' => ['string'],
            'per_page' => ['integer', 'in:10,25,50,100,200'],
            'currency' => ['string', 'in:R$,$,€,£,S$'],
        ];
    }
}
