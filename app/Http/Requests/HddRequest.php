<?php

namespace App\Http\Requests;

use App\Enums\TypeHddEnum;
use App\Enums\TypeStorageEnum;
use Illuminate\Validation\Rules\Enum;

class HddRequest extends AbstractRequest
{

    public function rules()
    {
        return [
            'quantity' => ['required', 'integer', 'min:1'],
            'type' => ['required', new Enum(TypeHddEnum::class)],
            'storage' => ['required', 'integer', 'min:1'],
            'type_storage' => ['required', new Enum(TypeStorageEnum::class)],
        ];
    }
}
