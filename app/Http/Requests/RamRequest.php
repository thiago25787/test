<?php

namespace App\Http\Requests;

use App\Enums\TypeStorageEnum;
use Illuminate\Validation\Rules\Enum;

class RamRequest extends AbstractRequest
{
    public function rules()
    {
        return [
            'storage' => ['required', 'integer', 'min:1'],
            'type_storage' => ['required', new Enum(TypeStorageEnum::class)],
            'ddr' => ['required', 'integer', 'min:1'],
        ];
    }
}
