<?php

namespace App\Http\Requests;

use App\DTOs\AbstractDTO;
use App\Exceptions\DTOException;
use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
