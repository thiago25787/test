<?php

namespace App\Http\Requests;

use App\Models\Hdd;
use App\Models\Location;
use App\Models\Model;
use App\Models\Ram;

class ServerRequest extends AbstractRequest
{
    public function rules()
    {
        return [
            'model_id' => ['required', 'integer', 'exists:' . Model::class . ',id'],
            'ram_id' => ['required', 'integer', 'exists:' . Ram::class . ',id'],
            'location_id' => ['required', 'integer', 'exists:' . Location::class . ',id'],
            'hdd_id' => ['required', 'integer', 'exists:' . Hdd::class . ',id'],
            'currency' => ['required', 'string', 'min:1', 'max:3'],
            'price' => ['required', 'numeric', 'min:1'],
        ];
    }
}
