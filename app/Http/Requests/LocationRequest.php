<?php

namespace App\Http\Requests;

class LocationRequest extends AbstractRequest
{
    public function rules()
    {
        return [
            'description' => ['required', 'string', 'max:255'],
        ];
    }
}
