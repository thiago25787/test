<?php

namespace App\Http\Requests;

class ModelRequest extends AbstractRequest
{
    public function rules()
    {
        return [
            'description' => ['required', 'string', 'max:255'],
        ];
    }
}
