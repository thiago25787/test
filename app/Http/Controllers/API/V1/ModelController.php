<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ModelRequest;
use App\Http\Resources\V1\ModelResource;
use App\Models\Model;
use Illuminate\Http\Response;

class ModelController extends Controller
{
    public function index()
    {
        $models = Model::all();
        return ModelResource::collection($models);
    }

    public function show(Model $model)
    {
        return new ModelResource($model);
    }

    public function store(ModelRequest $request)
    {
        $model = Model::create($request->all());
        return new ModelResource($model);
    }

    public function update(ModelRequest $request, Model $model)
    {
        $model->update($request->all());
        return new ModelResource($model);
    }

    public function destroy(Model $model)
    {
        $model->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

}
