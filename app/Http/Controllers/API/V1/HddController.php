<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\HddRequest;
use App\Http\Resources\V1\HddResource;
use App\Models\Hdd;
use Illuminate\Http\Response;

class HddController extends Controller
{
    public function index()
    {
        $hdds = Hdd::all();
        return HddResource::collection($hdds);
    }

    public function show(Hdd $hdd)
    {
        return new HddResource($hdd);
    }

    public function store(HddRequest $request)
    {
        $hdd = Hdd::create($request->all());
        return new HddResource($hdd);
    }

    public function update(HddRequest $request, Hdd $hdd)
    {
        $hdd->update($request->all());
        return new HddResource($hdd);
    }

    public function destroy(Hdd $hdd)
    {
        $hdd->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
