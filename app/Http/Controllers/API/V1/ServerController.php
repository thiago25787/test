<?php

namespace App\Http\Controllers\API\V1;

use App\Enums\TypeStorageEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServerListRequest;
use App\Http\Requests\ServerRequest;
use App\Http\Resources\V1\ServerResource;
use App\Models\Server;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class ServerController extends Controller
{
    public function index(ServerListRequest $request)
    {
        $servers = Server::search($request->all());
        return ServerResource::collection($servers);
    }

    public function show(Server $server)
    {
        return new ServerResource($server);
    }

    public function store(ServerRequest $request)
    {
        $server = Server::create($request->all());
        return new ServerResource($server);
    }

    public function update(ServerRequest $request, Server $server)
    {
        $server->update($request->all());
        return new ServerResource($server);
    }

    public function destroy(Server $server)
    {
        $server->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
