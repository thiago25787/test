<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\RamRequest;
use App\Http\Resources\V1\RamResource;
use App\Models\Ram;
use Illuminate\Http\Response;

class RamController extends Controller
{
    public function index()
    {
        $rams = Ram::all();
        return RamResource::collection($rams);
    }

    public function show(Ram $ram)
    {
        return new RamResource($ram);
    }

    public function store(RamRequest $request)
    {
        $ram = Ram::create($request->all());
        return new RamResource($ram);
    }

    public function update(RamRequest $request, Ram $ram)
    {
        $ram->update($request->all());
        return new RamResource($ram);
    }

    public function destroy(Ram $ram)
    {
        $ram->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
