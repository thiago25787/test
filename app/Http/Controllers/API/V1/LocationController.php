<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\LocationRequest;
use App\Http\Resources\V1\LocationResource;
use App\Models\Location;
use Illuminate\Http\Response;

class LocationController extends Controller
{
    public function index()
    {
        $locations = Location::all();
        return LocationResource::collection($locations);
    }

    public function show(Location $location)
    {
        return new LocationResource($location);
    }

    public function store(LocationRequest $request)
    {
        $location = Location::create($request->all());
        return new LocationResource($location);
    }

    public function update(LocationRequest $request, Location $location)
    {
        $location->update($request->all());
        return new LocationResource($location);
    }

    public function destroy(Location $location)
    {
        $location->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
