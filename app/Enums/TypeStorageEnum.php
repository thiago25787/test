<?php

namespace App\Enums;

enum TypeStorageEnum: string
{
    case GB = 'GB';
    case TB = 'TB';

    public static function toArray(): array
    {
        return array_column(self::cases(), 'value');
    }
}
