<?php

namespace App\Enums;

enum TypeHddEnum: string
{
    case SATA2 = 'SATA2';
    case SSD = 'SSD';
    case SAS = 'SAS';

    public static function toArray(): array
    {
        return array_column(self::cases(), 'value');
    }
}
